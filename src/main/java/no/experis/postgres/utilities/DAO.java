package no.experis.postgres.utilities;

import no.experis.postgres.models.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DAO {
    /**
     * DAO, Data Access Object, contains everything SQL.
     * This class can be instantiated to get access to database operations.
     * All SQL queries are executed here. The DAO keeps information of the database connection private.
     */
    private String URI = "jdbc:postgresql://ec2-54-80-184-43.compute-1.amazonaws.com:5432/d59fqdld700n1p?user=xcoewnrbwejdgw&password=5adbe17733339ee1c0d6d0ac421f87421ad3752ef06ef17561bb7b2de0db4891&sslmode=require";
    public Connection conn = null;

    public DAO() throws SQLException {
        //Make sure that we get the correct database
        if (System.getenv("JDBC_DATABASE_URL") != null) {
            URI = System.getenv("JDBC_DATABASE_URL");
        }
        this.openConn();
        System.out.println("Connection established...");

        this.initDatabase();
        System.out.println("Initialization complete!");
    }

    /**
     * Opens connection to database
     */
    public void openConn() {
        try {
            this.conn = DriverManager.getConnection(URI);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the mother of person in the database if motherId points to an existing person
     *
     * @param personId , person you want to update
     * @param motherId , person you want to reference as mother to person
     */
    public void setMother(int personId, int motherId) {
        try {
            String insert = "UPDATE person SET mother=? WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, motherId);
            preparedStatement.setInt(2, personId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("COULD NOT SET MOTHER, mother does not exist!");
        }
    }

    /**
     * Sets the father of person in the database if fatherId points to an existing person
     *
     * @param personId , person you want to update
     * @param fatherId , person you want to reference as father to person
     */
    public void setFather(int personId, int fatherId) {
        try {
            //Performs update query that sets father id of person
            String insert = "UPDATE person SET father=? WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, fatherId);
            preparedStatement.setInt(2, personId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("COULD NOT SET FATHER, father does not exist");
        }
    }

    /**
     * This method takes in information of a person and creates an instance of it in the database.
     * It connects person to a father and a mother if they exist in the database.
     * <p>
     * Parameters matches {@link Person} class
     *
     * @param first_name
     * @param last_name
     * @param dob
     * @param address
     * @param father
     * @param mother
     * @param phoneNumber
     * @param email
     * @return id of created person if successful, or -1 if unsuccessful
     * @throws SQLException, prints stacktrace
     */
    public int addPerson(String first_name, String last_name, String dob, String address, int father, int mother, String phoneNumber, String email) {
        try {
            //Check if the person has mother and father that exists
            if (father != 0 && getPersonById(father) == null) return -1;
            if (mother != 0 && getPersonById(mother) == null) return -1;

            //Creates a query for inserting a new row(person) to person table
            String insert = "INSERT INTO person (first_name, last_name, address, dob) VALUES (?,?,?,?) RETURNING id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, first_name);
            preparedStatement.setString(2, last_name);
            preparedStatement.setString(3, address);
            preparedStatement.setString(4, dob);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                //Get the id of the newly added person
                int id = result.getInt(1);
                //Add entries that are dependent existing.
                if (father != 0) setFather(id, father);
                if (mother != 0) setMother(id, mother);
                if (!phoneNumber.isEmpty()) addPhoneNumber(phoneNumber, id);
                if (!email.isEmpty()) addEmail(email, id);
                return id;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int updatePerson(int id, String first_name, String last_name, String dob, String address, int father, int mother) {
        try {
            if (father != 0 && getPersonById(father) == null) return -1;
            if (mother != 0 && getPersonById(mother) == null) return -1;

            String insert = "UPDATE person SET first_name=?, last_name=?, address=?, dob=? WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, first_name);
            preparedStatement.setString(2, last_name);
            preparedStatement.setString(3, address);
            preparedStatement.setString(4, dob);
            preparedStatement.setInt(5, id);
            preparedStatement.executeUpdate();
            setFather(id, father);
            setMother(id, mother);
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Uses other methods to create a Family class from {@link Family} based on the person found by input id.
     *
     * @param id , the person that Family is based around
     * @return new Family object with all related person in database
     */
    public Family getFamily(int id) {
        return new Family(getMother(id), getFather(id), getSiblings(id), getChildren(id));
    }

    private Person getFather(int childId) {
        return getPersonById(getPersonById(childId).getFather());
    }

    private Person getMother(int childId) {
        return getPersonById(getPersonById(childId).getMother());
    }

    /**
     * Gets all persons in database ordered by id.
     * All persons are instantiated by {@link Person} class.
     *
     * @return ArrayList of Person objects
     */
    public ArrayList<Person> getAllPersons() {
        ArrayList<Person> personList = new ArrayList<>();
        try {
            String insert = "SELECT * FROM person ORDER  BY id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                personList.add(
                        new Person(
                                resultSet.getInt("id"),
                                resultSet.getInt("father"),
                                resultSet.getInt("mother"),
                                resultSet.getString("first_name"),
                                resultSet.getString("last_name"),
                                resultSet.getString("dob"),//"2002-02-11 00:11:22"
                                resultSet.getString("address")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personList;
    }

    /**
     * This method finds siblings of the person matching childId by querying the database.
     * It uses it's father id and mother id to match with existing persons and returns them in an array.
     *
     * @param childId , the id of the person whose siblings you want to find
     * @return array of persons, or null if no match/SQL error
     */
    private Person[] getSiblings(int childId) {
        //Gets the persons father and mother id
        int fatherId = getPersonById(childId).getFather();
        int motherId = getPersonById(childId).getMother();
        List<Person> personList = new ArrayList();

        try {
            String insert = "SELECT * FROM person WHERE (father=? OR mother=?) AND id!=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, fatherId);
            preparedStatement.setInt(2, motherId);
            preparedStatement.setInt(3, childId);

            //Executes query and returns it in a ResultSet
            ResultSet result = preparedStatement.executeQuery();

            //Parses through ResultSet and adds each person to personList
            while (result.next()) {
                personList.add(getPersonById(result.getInt("id")));
            }

            //Sets an array personArray equal to personList
            Person[] personArray = new Person[personList.size()];
            for (int i = 0; i < personList.size(); i++) {
                personArray[i] = personList.get(i);
            }
            return personArray;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Finds all children of the person passed as parentId. It uses parentId and checks all
     * persons mother and father to see if there is a match.
     *
     * @param parentId , the id of the person whose children you want to find
     * @return array of persons, or null if no match/SQL error
     */
    public Person[] getChildren(int parentId) {
        List<Person> personList = new ArrayList();
        try {
            //Creates a query that retrieves all persons with a mother or father matching parentId
            String insert = "SELECT * FROM person WHERE mother=? OR father=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, parentId);
            preparedStatement.setInt(2, parentId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                personList.add(getPersonById(result.getInt("id")));
            }
            Person[] personArray = new Person[personList.size()];
            for (int i = 0; i < personList.size(); i++) {
                personArray[i] = personList.get(i);
            }
            return personArray;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PersonInformation getPersonInformation(int id) {
        return new PersonInformation(getPersonById(id), getFamily(id), getEmailsByPersonId(id), getPhoneNumbersByPersonId(id));
    }

    /**
     * Deletes person with selected id from database.
     * Cascades on phone numbers and emails and removes other persons relation to this id.
     *
     * @param id
     */
    public void deletePersonById(int id) {
        try {
            String insert = "DELETE FROM person WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes email with selected id from database.
     *
     * @param id
     */
    public void deleteEmailById(int id) {
        try {
            String insert = "DELETE FROM email WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes phone number with selected id from database.
     *
     * @param id
     */
    public void deletePhoneNumberById(int id) {
        try {
            String insert = "DELETE FROM phone_number WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a new email address to database with relation to given personId.
     *
     * @param email    , the new email to be added to person
     * @param personId , the id of person the email will relate to
     */
    public int addEmail(String email, int personId) {
        try {
            String insert = "INSERT INTO email (email, personID) VALUES (?,?) RETURNING id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, email);
            preparedStatement.setInt(2, personId);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                int id = result.getInt(1);
                return id;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public Email getEmailById(int id) {
        try {
            String insert = "SELECT * FROM email WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Email(result.getInt("id"), result.getInt("personID"), result.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Retrieves all the phone numbers of a person
     *
     * @param personId , the person whose phone numbers you want
     * @return ArrayList with all the numbers
     */
    public ArrayList<PhoneNumber> getPhoneNumbersByPersonId(int personId) {
        ArrayList<PhoneNumber> numberList = new ArrayList<>();
        try {
            String insert = "SELECT * FROM phone_number WHERE personID=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, personId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                numberList.add(new PhoneNumber(result.getInt("id"), result.getInt("personID"), result.getString("number")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return numberList;
    }

    /**
     * Retrieves all the emails of a person
     *
     * @param personId , the person whose emails you want
     * @return ArrayList with all the emails
     */
    public ArrayList<Email> getEmailsByPersonId(int personId) {
        ArrayList<Email> emailList = new ArrayList<>();
        try {
            String insert = "SELECT * FROM email WHERE personID=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, personId);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                emailList.add(new Email(result.getInt("id"), result.getInt("personID"), result.getString("email")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return emailList;
    }

    /**
     * Adds a new phone number to database with relation to given personId.
     *
     * @param phoneNumber , the new phone number to be added to person
     * @param personId    , the id of person the phone number will relate to
     */
    public int addPhoneNumber(String phoneNumber, int personId) {
        try {
            String insert = "INSERT INTO phone_number (number, personID) VALUES (?,?) RETURNING id";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, phoneNumber);
            preparedStatement.setInt(2, personId);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                int id = result.getInt(1);
                return id;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public PhoneNumber getPhoneNumberById(int id) {
        try {
            String insert = "SELECT * FROM phone_number WHERE id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new PhoneNumber(result.getInt("id"), result.getInt("personID"), result.getString("number"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Used to search for persons by email. Requires exact match on email.
     *
     * @param email
     * @return Person with the given email
     */
    public Person getPersonByEmail(String email) {
        try {
            String insert = "SELECT * FROM email JOIN person ON email.personId=person.id where email=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, email);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Person(result.getInt("id"), result.getInt("father"), result.getInt("mother"),
                        result.getString("first_name"), result.getString("last_name"),
                        result.getString("dob"), result.getString("address"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Used to search for persons by phone number. Requires exact match on phone number.
     *
     * @param phoneNumber
     * @return Person with the given phone number
     */
    public Person getPersonByPhoneNr(String phoneNumber) {
        try {
            String insert = "SELECT * FROM phone_number JOIN person ON phone_number.personId=person.id where number=?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setString(1, phoneNumber);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Person(result.getInt("id"), result.getInt("father"), result.getInt("mother"),
                        result.getString("first_name"), result.getString("last_name"),
                        result.getString("dob"), result.getString("address"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Used to search for persons by id. Requires exact match on id
     *
     * @param id
     * @return Person object with the given id
     */
    public Person getPersonById(int id) {
        try {
            String insert = "SELECT * FROM person where id = ?";
            PreparedStatement preparedStatement = conn.prepareStatement(insert);
            preparedStatement.setInt(1, id);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return new Person(result.getInt("id"), result.getInt("father"), result.getInt("mother"),
                        result.getString("first_name"), result.getString("last_name"),
                        result.getString("dob"), result.getString("address"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Initializes database by checking if the required tables exists.
     * Creates new tables if they do not exist.
     */
    private void initDatabase() {
        try {

            PreparedStatement personPreparedStatement = conn.prepareStatement("CREATE TABLE IF NOT EXISTS person(id SERIAL PRIMARY KEY, first_name varchar, last_name varchar, address varchar, mother int references person(id) ON DELETE SET NULL, father int references person(id) ON DELETE SET NULL, dob varchar)");
            personPreparedStatement.execute();
            PreparedStatement phonePreparedStatement = conn.prepareStatement("CREATE TABLE IF NOT EXISTS phone_number(id SERIAL PRIMARY KEY, number varchar, personID int REFERENCES person(id) ON DELETE CASCADE )");
            phonePreparedStatement.execute();
            PreparedStatement emailPreparedStatement = conn.prepareStatement("CREATE TABLE IF NOT EXISTS email(id SERIAL PRIMARY KEY, email varchar, personID int REFERENCES person(id) ON DELETE CASCADE)");
            emailPreparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
