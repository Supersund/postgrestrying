package no.experis.postgres;

import no.experis.postgres.utilities.DAO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;

@SpringBootApplication
public class PostgresApplication {

    public static void main(String[] args) throws SQLException {
        DAO dao = new DAO();
        dao.deletePersonById(1);

        SpringApplication.run(PostgresApplication.class, args);
    }
}
