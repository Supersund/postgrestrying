package no.experis.postgres.controllers;


import no.experis.postgres.models.*;
import no.experis.postgres.utilities.DAO;
import no.experis.postgres.PostgresApplication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@RestController
public class PersonController {

    DAO dao = new DAO();

    public PersonController() throws SQLException {
        dao.openConn();
        //dao.readPersons();
    }

    @CrossOrigin
    @RequestMapping("/person")
    public ArrayList<Person> personFind(@RequestParam(value = "ID", defaultValue = "1") String ID) {

        ArrayList<Person> returnPersons = new ArrayList<Person>();
        System.out.println("Trying to find person2: " + ID);


        try {
            returnPersons = dao.getAllPersons();
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return returnPersons;
    }

    @CrossOrigin
    @GetMapping("/person/{ID}")
    public PersonInformation personGet(@PathVariable String ID) {
        System.out.println("Trying to find person with ID: " + ID);
        PersonInformation returnPerson = null;

        try {
            returnPerson = dao.getPersonInformation(Integer.parseInt(ID));
            System.out.println("Found person:" + returnPerson.toString());
        } catch (Exception e) {
            System.out.println("Not found!");
            System.out.println(e);
        }
        return returnPerson;
    }

    @CrossOrigin
    @GetMapping("/family/{ID}")
    public Family familyGet(@PathVariable String ID) {
        System.out.println("Trying to find people in family with ID: " + ID);
        Family returnFamily = null;

        try {
            returnFamily = dao.getFamily(Integer.parseInt(ID));
            System.out.println("Found person:" + returnFamily.toString());
        } catch (Exception e) {
            System.out.println("Not found!");
            System.out.println(e);
        }
        return returnFamily;
    }

    @CrossOrigin
    @GetMapping("/email/{ID}")
    public Person getPersonsByEmail(@PathVariable String ID) {
        System.out.println("Trying to find people with email: " + ID);
        Person returnPerson = null;

        try {
            returnPerson = dao.getPersonByEmail(ID);
            System.out.println("Found person:" + returnPerson.toString());
        } catch (Exception e) {
            System.out.println("Not found!");
            System.out.println(e);
        }
        return returnPerson;
    }

    @CrossOrigin
    @GetMapping("/phonenumber/{ID}")
    public Person getPersonsByPhonenumber(@PathVariable String ID) {
        System.out.println("Trying to find people with phonenumber: " + ID);
        Person returnPerson = null;

        try {
            returnPerson = dao.getPersonByPhoneNr(ID);
            System.out.println("Found person:" + returnPerson.toString());
        } catch (Exception e) {
            System.out.println("Not found!");
            System.out.println(e);
        }
        return returnPerson;
    }

    @CrossOrigin
    @GetMapping("/family")
    public String getFamily(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "family";
    }

    @CrossOrigin
    @PostMapping("/phonenumber")
    public String postPhoneNumber(@RequestParam(name = "number", required = true, defaultValue = "0") String phoneNumber,
                                  @RequestParam(name = "personId", required = true, defaultValue = "-1") String personId) {
        int _personId = -1;
        try {
            _personId = Integer.parseInt(personId);
            int phoneId = dao.addPhoneNumber(phoneNumber, _personId);
            return String.valueOf(phoneId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "-1";
    }

    @CrossOrigin
    @DeleteMapping("/phonenumber/{ID}")
    public PhoneNumber deletePhoneNumber(@PathVariable String ID) {
        PhoneNumber phoneNumber = dao.getPhoneNumberById(Integer.parseInt(ID));
        dao.deletePhoneNumberById(Integer.parseInt(ID));
        return phoneNumber;
    }


    @CrossOrigin
    @PostMapping("/email")
    public String postEmail(@RequestParam(name = "email", required = true, defaultValue = "") String email,
                            @RequestParam(name = "personId", required = true, defaultValue = "-1") String personId) {
        int _personId = -1;
        try {
            _personId = Integer.parseInt(personId);
            int emailId = dao.addEmail(email, _personId);
            return String.valueOf(emailId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "-1";
    }

    @CrossOrigin
    @PostMapping("/mother")
    public String setMother(@RequestParam(name = "motherId", required = true, defaultValue = "") String motherId,
                            @RequestParam(name = "personId", required = true, defaultValue = "-1") String personId) {
        int _personId = -1;
        int _motherId = -1;
        try {
            _personId = Integer.parseInt(personId);
            _motherId = Integer.parseInt(motherId);
            dao.setMother(_personId, _motherId);
            return String.valueOf(_motherId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "-1";
    }

    @CrossOrigin
    @PostMapping("/father")
    public String setFather(@RequestParam(name = "fatherId", required = true, defaultValue = "") String fatherId,
                            @RequestParam(name = "personId", required = true, defaultValue = "-1") String personId) {
        int _personId = -1;
        int _fatherId = -1;
        try {
            _personId = Integer.parseInt(personId);
            _fatherId = Integer.parseInt(fatherId);
            dao.setFather(_personId, _fatherId);
            return String.valueOf(_fatherId);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return "-1";
    }

    @CrossOrigin
    @DeleteMapping("/email/{ID}")
    public Email deleteMail(@PathVariable String ID) {
        Email phoneNumber = dao.getEmailById(Integer.parseInt(ID));
        dao.deleteEmailById(Integer.parseInt(ID));
        return phoneNumber;
    }

    @CrossOrigin
    @PutMapping("/person")
    public String postPerson(@RequestParam(name = "id", required = true, defaultValue = "0") String id,
                             @RequestParam(name = "firstname", required = true, defaultValue = "") String firstname,
                             @RequestParam(name = "lastname", required = true, defaultValue = "") String lastname,
                             @RequestParam(name = "dob", required = true, defaultValue = "") String dob,
                             @RequestParam(name = "address", required = true, defaultValue = "") String address,
                             @RequestParam(name = "father", required = true, defaultValue = "0") String father,
                             @RequestParam(name = "mother", required = true, defaultValue = "0") String mother) {

        int _mother = 0;
        int _father = 0;
        try {
            _mother = Integer.parseInt(mother);
            _father = Integer.parseInt(father);
        } catch (NumberFormatException ex) {
            System.out.println("Mother/Father values not valid number: " + ex);
            return "-1";
        }
        int _id = 0;
        try {
            _id = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            System.out.println("Id not a valid number: " + e);
            return "-1";
        }
        int personId = -1;

        try {
            personId = dao.updatePerson(_id, firstname, lastname, dob, address, _father, _mother);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(personId);

    }

    @CrossOrigin
    @PostMapping("/person")
    public String postPerson(@RequestParam(name = "firstname", required = true, defaultValue = "") String firstname,
                             @RequestParam(name = "lastname", required = true, defaultValue = "") String lastname,
                             @RequestParam(name = "dob", required = true, defaultValue = "") String dob,
                             @RequestParam(name = "address", required = true, defaultValue = "") String address,
                             @RequestParam(name = "father", required = true, defaultValue = "0") String father,
                             @RequestParam(name = "mother", required = true, defaultValue = "0") String mother,
                             @RequestParam(name = "email", required = true, defaultValue = "") String email,
                             @RequestParam(name = "phonenumber", required = true, defaultValue = "") String phonenumber
    ) {
        int _mother = 0;
        int _father = 0;
        try {
            _mother = Integer.parseInt(mother);
            _father = Integer.parseInt(father);
        } catch (NumberFormatException ex) {
            System.out.println("Mother/Father values not valid number: " + ex);
            return "-1";
        }
        int personId = -1;
        try {
            personId = dao.addPerson(firstname, lastname, dob, address, _father, _mother, phonenumber, email);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(personId);
    }

    @CrossOrigin
    @DeleteMapping("/person/{ID}")
    public Person deletePerson(@PathVariable String ID) {
        Person returnPerson = dao.getPersonById(Integer.parseInt(ID));
        dao.deletePersonById(Integer.parseInt(ID));

        return returnPerson;
    }
}
