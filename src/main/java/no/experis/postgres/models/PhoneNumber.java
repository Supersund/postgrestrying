package no.experis.postgres.models;

public class PhoneNumber {
    private int id;
    private int personId;
    private String phoneNumber;

    public PhoneNumber(int id, int personId, String phoneNumber) {
        this.id = id;
        this.personId = personId;
        this.phoneNumber = phoneNumber;
    }

    public int getId() {
        return id;
    }

    public int getPersonId() {
        return personId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
