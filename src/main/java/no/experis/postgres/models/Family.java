package no.experis.postgres.models;

import java.util.Arrays;

public class Family {
    private Person mother;
    private Person father;
    private Person[] siblings;
    private Person[] children;

    public Family(Person mother, Person father, Person[] siblings, Person[] children) {
        this.mother = mother;
        this.father = father;
        this.siblings = siblings;
        this.children = children;
    }

    public Person getMother() {
        return mother;
    }

    public Person getFather() {
        return father;
    }

    public Person[] getSiblings() {
        return siblings;
    }

    public Person[] getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", siblings=" + Arrays.toString(siblings) +
                ", children=" + Arrays.toString(children) +
                '}';
    }
}
