package no.experis.postgres.models;

import java.util.ArrayList;

public class PersonInformation {
    private Person person;
    private Family family;
    private ArrayList<Email> emails;
    private ArrayList<PhoneNumber> phoneNumbers;

    public PersonInformation(Person person, Family family, ArrayList<Email> emails, ArrayList<PhoneNumber> phoneNumbers) {
        this.person = person;
        this.family = family;
        this.emails = emails;
        this.phoneNumbers = phoneNumbers;
    }

    public Person getPerson() {
        return person;
    }

    public Family getFamily() {
        return family;
    }

    public ArrayList<Email> getEmails() {
        return emails;
    }

    public ArrayList<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    @Override
    public String toString() {
        return "PersonInformation{" +
                "person=" + person +
                ", family=" + family +
                ", emails=" + emails +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    }
}
