package no.experis.postgres.models;

public class Person {

    private int id;
    private int father;
    private int mother;
    private String first_name;
    private String last_name;
    private String dob;
    private String address;
    private String img;

    public Person(int id, int father, int mother, String first_name, String last_name, String dob, String address) {
        this.id = id;
        this.father = father;
        this.mother = mother;
        this.first_name = first_name;
        this.last_name = last_name;
        this.dob = dob;
        this.address = address;
        this.img = "https://randomuser.me/api/portraits/men/" + id % 100 + ".jpg";
    }

    public int getId() {
        return id;
    }

    public int getFather() {
        return father;
    }

    public int getMother() {
        return mother;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getDob() {
        return dob;
    }

    public String getAddress() {
        return address;
    }

    public String getImg() {
        return img;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", father=" + father +
                ", mother=" + mother +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", dob='" + dob + '\'' +
                ", address='" + address + '\'' +
                ", img='" + img + '\'' +
                '}';
    }
}
