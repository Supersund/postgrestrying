package no.experis.postgres.models;

public class Email {
    private int id;
    private int personId;
    private String email;

    public Email(int id, int personId, String email) {
        this.id = id;
        this.personId = personId;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public int getPersonId() {
        return personId;
    }

    public String getEmail() {
        return email;
    }
}
